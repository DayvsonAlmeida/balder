const expertiseModel = require("../../models/expertise");
const { SUCCESS, SERVER_ERROR } = require("../../utils/constants/status-code");

const route = async (req, res) => {
  try {
    const result = await expertiseModel.find({});
    const noContent = result.length === 0;
    const status = noContent ? SUCCESS.NO_CONTENT : SUCCESS.OK;

    return res.status(status).json({ expertises: result });
  } catch (err) {
    return res
      .status(SERVER_ERROR.INTERNAL_SERVER_ERROR)
      .json({ message: "Can't find expertises" });
  }
};

module.exports = route;
