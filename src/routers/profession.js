const { Router } = require("express");

const auth = require("../middlewares/auth");

const list = require("../routes/profession/list");
const create = require("../routes/profession/create");
const update = require("../routes/profession/update");
const remove = require("../routes/profession/remove");

const router = Router();

router.use(auth);

router.get("/", list);
router.post("/", create);
router.put("/", update);
router.delete("/", remove);

module.exports = router;
