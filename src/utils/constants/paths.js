module.exports = {
  BASE_PATH: "/api/v1",
  PROFESSIONAL_PATH: "/professional",
  PROFESSION_PATH: "/profession",
  EXPERTISE_PATH: "/expertise",
  COUNCIL_PATH: "/council",
  LOGIN_PATH: "/login",
};
