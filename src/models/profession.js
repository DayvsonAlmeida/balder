const mongoose = require("mongoose");

const schema = new mongoose.Schema({
  name: { type: String, required: true, unique: true },
});

const model = mongoose.model("professions", schema);

module.exports = model;
