const { Router } = require("express");

const auth = require("../middlewares/auth");

const list = require("../routes/expertise/list");
const create = require("../routes/expertise/create");
const update = require("../routes/expertise/update");
const remove = require("../routes/expertise/remove");

const router = Router();

router.use(auth);

router.get("/", list);
router.post("/", create);
router.put("/", update);
router.delete("/", remove);

module.exports = router;
