const professionModel = require("../../models/profession");
const { SUCCESS, SERVER_ERROR } = require("../../utils/constants/status-code");

const route = async (req, res) => {
  const { id } = req.body;

  try {
    const result = await professionModel.findByIdAndRemove(id);

    return res
      .status(SUCCESS.OK)
      .json({ message: `Profession with id ${id} deleted`, result });
  } catch (err) {
    return res
      .status(SERVER_ERROR.INTERNAL_SERVER_ERROR)
      .json({ message: `Can't delete profession with id ${id}` });
  }
};

module.exports = route;
