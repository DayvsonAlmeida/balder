const expertiseModel = require("../../models/expertise");
const { SUCCESS, SERVER_ERROR } = require("../../utils/constants/status-code");

const route = async (req, res) => {
  const { id, ...data } = req.body;

  try {
    const result = await expertiseModel.findByIdAndUpdate(id, data, {
      new: true,
    });

    return res.status(SUCCESS.OK).json(result);
  } catch (err) {
    return res
      .status(SERVER_ERROR.INTERNAL_SERVER_ERROR)
      .json({ message: `Can't update expertise with id ${id}` });
  }
};

module.exports = route;
