const { Router } = require("express");

const auth = require("../middlewares/auth");

const list = require("../routes/council/list");
const create = require("../routes/council/create");
const update = require("../routes/council/update");
const remove = require("../routes/council/remove");

const router = Router();

router.use(auth);

router.get("/", list);
router.post("/", create);
router.put("/", update);
router.delete("/", remove);

module.exports = router;
