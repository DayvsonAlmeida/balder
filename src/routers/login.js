const { Router } = require("express");

const login = require("../routes/login");

const router = Router();

router.post("/", login);

module.exports = router;
